# README #


This repository contains a STM32L051XX ARM M0+ mpu blank embed project for the [em::blocks](http://www.emblocks.org/web/) ide.

[Learn mBed](http://developer.mbed.org/)

[mBed SDK](https://github.com/mbedmicro/mbed)

[STM32L051xx datasheet](http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/DM00108219.pdf)


Clone this repository in a folder an the open the project with the ide.
